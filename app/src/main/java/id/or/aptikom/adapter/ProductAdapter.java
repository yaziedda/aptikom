package id.or.aptikom.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.model.MsNews;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.util.IDRUtils;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    List<MsProduct> list = new ArrayList<>();

    public interface OnItemClickListener {
        void onItemClick(MsProduct model);
    }

    private final OnItemClickListener listener;

    public ProductAdapter(List<MsProduct> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MsProduct model = list.get(position);

        if(model.getImage() != null && !model.getImage().isEmpty()){
            Glide.with(holder.itemView.getContext())
                    .load(model.getImage()).into(holder.ivImage);
        }else{
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_launcher).into(holder.ivImage);
        }

        holder.tvTitle.setText(model.getTitle());

        Spanned spanned = Html.fromHtml(model.getDesc());
        holder.tvContent.setText(spanned);
        holder.tvPrice.setText(IDRUtils.toRupiah(model.getPrice()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        });

        holder.tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_image)
        ImageView ivImage;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_content)
        TextView tvContent;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_more)
        TextView tvMore;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

