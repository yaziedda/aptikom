package id.or.aptikom.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.model.TrxProduct;
import id.or.aptikom.util.IDRUtils;

public class TrxProductAdapater extends RecyclerView.Adapter<TrxProductAdapater.ViewHolder> {

    List<TrxProduct> list = new ArrayList<>();


    public interface OnItemClickListener {
        void onItemClick(TrxProduct model);
    }

    private final OnItemClickListener listener;

    public TrxProductAdapater(List<TrxProduct> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trx_payment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final TrxProduct model = list.get(position);

        holder.tvTicket.setText("Ticket ID "+model.getTicketId());
        holder.tvAmount.setText(IDRUtils.toRupiah(model.getAmount()));
        holder.tvPTitle.setText(model.getProductTitle());
        holder.tvStatus.setText(model.getStatusName());
        holder.tvStatus.setTextColor(Color.parseColor("#"+model.getStatusColor()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_ticket)
        TextView tvTicket;
        @BindView(R.id.tv_amount)
        TextView tvAmount;
        @BindView(R.id.tv_p_title)
        TextView tvPTitle;
        @BindView(R.id.tv_status)
        TextView tvStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

