package id.or.aptikom.client;

import android.content.Context;

import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.util.Static;


public class ApiUtils {

    public static String API = Static.BASE_URL;

    public static MobileService MobileService(Context context){
        return RetrofitClient.getClient(context, API).create(MobileService.class);
    }


}
