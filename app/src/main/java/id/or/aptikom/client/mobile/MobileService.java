package id.or.aptikom.client.mobile;


import java.util.Map;


import id.or.aptikom.client.model.Response;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface MobileService {

    @FormUrlEncoded
    @POST("auth-persone")
    Call<Response> authPersone(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("auth-persone-pwd")
    Call<Response> authPersonePwd(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("auth-persone-first-pwd")
    Call<Response> authPersoneFirstPwd(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("profile-edit/{id}")
    Call<Response> profileEdit(@Path("id") long id, @FieldMap Map<String, String> map);

    @GET("ms-config")
    Call<Response> getMsConfig();

    @GET("ms-news")
    Call<Response> getMsNews();

    @GET("ms-news/{id}")
    Call<Response> getMsNewsDetail(@Path("id") long id);

    @GET("ms-headline")
    Call<Response> getMsHeadline();

    @GET("ms-product")
    Call<Response> getMsProduct();

    @GET("ms-product/{id}")
    Call<Response> getMsProductDetail(@Path("id") long id);

    @FormUrlEncoded
    @POST("product-payment")
    Call<Response> payProduct(@FieldMap Map<String, String> map);

    @GET("product-payment/{id}")
    Call<Response> payProductDetail(@Path("id") long id);

    @GET("my-payment/{id}")
    Call<Response> myPayment(@Path("id") long id);

    @Multipart
    @POST("upload-bukti-tf")
    Call<Response> uploadBuktiTF(@Part MultipartBody.Part img, @PartMap Map<String, RequestBody> partMap);

}
