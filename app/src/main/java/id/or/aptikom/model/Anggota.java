package id.or.aptikom.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Anggota {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kode_wilayah")
    @Expose
    private Integer kodeWilayah;
    @SerializedName("no_anggota")
    @Expose
    private String noAnggota;
    @SerializedName("persone")
    @Expose
    private String persone;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("perguruan_tinggi")
    @Expose
    private String perguruanTinggi;
    @SerializedName("jabatan")
    @Expose
    private String jabatan;
    @SerializedName("program_studi")
    @Expose
    private String programStudi;
    @SerializedName("alamat_perguruan_tinggi")
    @Expose
    private String alamatPerguruanTinggi;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("via")
    @Expose
    private String via;
    @SerializedName("masa_berlaku")
    @Expose
    private String masaBerlaku;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKodeWilayah() {
        return kodeWilayah;
    }

    public void setKodeWilayah(Integer kodeWilayah) {
        this.kodeWilayah = kodeWilayah;
    }

    public String getNoAnggota() {
        return noAnggota;
    }

    public void setNoAnggota(String noAnggota) {
        this.noAnggota = noAnggota;
    }

    public String getPersone() {
        return persone;
    }

    public void setPersone(String persone) {
        this.persone = persone;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPerguruanTinggi() {
        return perguruanTinggi;
    }

    public void setPerguruanTinggi(String perguruanTinggi) {
        this.perguruanTinggi = perguruanTinggi;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getProgramStudi() {
        return programStudi;
    }

    public void setProgramStudi(String programStudi) {
        this.programStudi = programStudi;
    }

    public String getAlamatPerguruanTinggi() {
        return alamatPerguruanTinggi;
    }

    public void setAlamatPerguruanTinggi(String alamatPerguruanTinggi) {
        this.alamatPerguruanTinggi = alamatPerguruanTinggi;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getMasaBerlaku() {
        return masaBerlaku;
    }

    public void setMasaBerlaku(String masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
