package id.or.aptikom.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MsConfig {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sejarah")
    @Expose
    private String sejarah;
    @SerializedName("struktur_org")
    @Expose
    private String strukturOrg;
    @SerializedName("visi_misi")
    @Expose
    private String visiMisi;
    @SerializedName("peraturan")
    @Expose
    private String peraturan;
    @SerializedName("bank_no_rek")
    @Expose
    private String bankNoRek;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_an")
    @Expose
    private String bankAn;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSejarah() {
        return sejarah;
    }

    public void setSejarah(String sejarah) {
        this.sejarah = sejarah;
    }

    public String getStrukturOrg() {
        return strukturOrg;
    }

    public void setStrukturOrg(String strukturOrg) {
        this.strukturOrg = strukturOrg;
    }

    public String getVisiMisi() {
        return visiMisi;
    }

    public void setVisiMisi(String visiMisi) {
        this.visiMisi = visiMisi;
    }

    public String getPeraturan() {
        return peraturan;
    }

    public void setPeraturan(String peraturan) {
        this.peraturan = peraturan;
    }

    public String getBankNoRek() {
        return bankNoRek;
    }

    public void setBankNoRek(String bankNoRek) {
        this.bankNoRek = bankNoRek;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAn() {
        return bankAn;
    }

    public void setBankAn(String bankAn) {
        this.bankAn = bankAn;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
