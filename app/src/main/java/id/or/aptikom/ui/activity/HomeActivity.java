package id.or.aptikom.ui.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.adapter.SectionPagerAdapter;
import id.or.aptikom.ui.fragment.AccountFragment;
import id.or.aptikom.ui.fragment.MyAptikomFragment;
import id.or.aptikom.ui.fragment.HomeFragment;
import id.or.aptikom.ui.fragment.NewsFragment;
import id.or.aptikom.ui.fragment.SettingFragment;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    MenuItem prevMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        final SectionPagerAdapter sectionsPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        sectionsPagerAdapter.addFragment(new HomeFragment());
        sectionsPagerAdapter.addFragment(new AccountFragment());
        sectionsPagerAdapter.addFragment(new NewsFragment());
        sectionsPagerAdapter.addFragment(new MyAptikomFragment());
        sectionsPagerAdapter.addFragment(new SettingFragment());
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null)
                    prevMenuItem.setChecked(false);
                else
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.action_account:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.action_news:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.action_my_aptikom:
                                viewPager.setCurrentItem(3);
                                break;
                            case R.id.action_setting:
                                viewPager.setCurrentItem(4);
                                break;
//                            case R.id.action_setting:
//                                viewPager.setCurrentItem(4);
//                                break;
                        }
                        return false;
                    }
                });


    }
}
