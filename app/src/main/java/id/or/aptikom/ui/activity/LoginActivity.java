package id.or.aptikom.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.util.BaseActivity;
import id.or.aptikom.util.Preferences;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.ll_login)
    LinearLayout llLogin;
    @BindView(R.id.ll_choice)
    LinearLayout llChoice;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.bt_perorangan)
    Button btPerorangan;
    @BindView(R.id.bt_institusi)
    Button btInstitusi;
    @BindView(R.id.tv_no_anggota)
    TextInputEditText tvNoAnggota;
    @BindView(R.id.tv_pwd)
    TextInputEditText tvPwd;
    @BindView(R.id.tv_pwd_confirm)
    TextInputEditText tvPwdConfirm;
    MobileService mobileService;
    @BindView(R.id.ti_pwd)
    TextInputLayout tiPwd;
    @BindView(R.id.ti_pwd_confirm)
    TextInputLayout tiPwdConfirm;
    @BindView(R.id.ti_no_anggota)
    TextInputLayout tiNoAnggota;
    @BindView(R.id.tv_message)
    TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        llLogin.setVisibility(View.GONE);
        llChoice.setVisibility(View.GONE);
        tvPwd.setVisibility(View.GONE);
        tvPwdConfirm.setVisibility(View.GONE);
        tiPwd.setVisibility(View.GONE);
        tiPwdConfirm.setVisibility(View.GONE);
        tvMessage.setVisibility(View.GONE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(Preferences.getLoginFlag(getApplicationContext())){
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                finish();
                            }else{
                                llChoice.setVisibility(View.VISIBLE);
                                llChoice.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_out));
                            }
                        }
                    });
                }
            }
        }).start();

        btPerorangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llChoice.setVisibility(View.GONE);
                llLogin.setVisibility(View.VISIBLE);
                llLogin.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_out));
            }
        });

        btInstitusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                llChoice.setVisibility(View.GONE);
//                llLogin.setVisibility(View.VISIBLE);
//                llLogin.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.push_out));
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPwdConfirm.setVisibility(View.GONE);
                tvPwd.setVisibility(View.GONE);
                tiPwd.setVisibility(View.GONE);
                tiPwdConfirm.setVisibility(View.GONE);
                final String noAnggota = tvNoAnggota.getText().toString();
                if (!noAnggota.isEmpty()) {
                    Map<String, String> params = new HashMap<>();
                    params.put("no_anggota", noAnggota);
                    showPleasewaitDialog();
                    mobileService.authPersone(params).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            dissmissPleasewaitDialog();
                            Response body = response.body();
                            if (body != null && body.getData() != null) {
                                if (body.isState()) {
                                    authPassword(noAnggota);
                                } else {
                                    double state = (double) body.getData();
                                    if (state == 1) {
                                        createPassword(noAnggota);
                                    }
                                }
                            } else {
                                showMessage("No Anggota tidak valid");
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            dissmissPleasewaitDialog();
                            showMessage("Terjadi kesalahan, coba beberapa saat lagi");
                        }
                    });
                } else {
                    showMessage("Tidak boleh kosong");
                }
            }
        });


    }

    private void authPassword(final String noAnggota) {
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText("Masukan password");

        tiPwd.setVisibility(View.VISIBLE);
        tvPwd.setVisibility(View.VISIBLE);

        tiPwdConfirm.setVisibility(View.GONE);
        tvPwdConfirm.setVisibility(View.GONE);

        tiNoAnggota.setVisibility(View.GONE);
        tvNoAnggota.setVisibility(View.GONE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pwd = tvPwd.getText().toString();
                Map<String, String> params = new HashMap<>();
                params.put("no_anggota", noAnggota);
                params.put("password", pwd);
                showPleasewaitDialog();
                mobileService.authPersonePwd(params).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        Response body = response.body();
                        if (body.getData() != null) {
                            if (body.isState()) {
                                goToHome(body);
                            } else {
                                showMessage("Password salah");
                            }
                        } else {
                            showMessage("Password salah");
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage("Terjadi kesalahan, coba beberapa saat lagi");
                    }
                });
            }
        });
    }

    private void createPassword(final String noAnggota) {
        tvMessage.setVisibility(View.VISIBLE);
        tvMessage.setText("Buat password anda");

        tiPwd.setVisibility(View.VISIBLE);
        tvPwd.setVisibility(View.VISIBLE);

        tiPwdConfirm.setVisibility(View.VISIBLE);
        tvPwdConfirm.setVisibility(View.VISIBLE);

        tiNoAnggota.setVisibility(View.GONE);
        tvNoAnggota.setVisibility(View.GONE);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pwd = tvPwd.getText().toString();
                String pwdConfirm = tvPwdConfirm.getText().toString();

                if (!pwd.equals(pwdConfirm)) {
                    showMessage("Password tidak sama");
                    return;
                } else {
                    Map<String, String> params = new HashMap<>();
                    params.put("no_anggota", noAnggota);
                    params.put("password", pwd);
                    showPleasewaitDialog();
                    mobileService.authPersoneFirstPwd(params).enqueue(new Callback<Response>() {
                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            dissmissPleasewaitDialog();
                            Response body = response.body();
                            if (body.getData() != null) {
                                if (body.isState()) {
                                    goToHome(body);
                                } else {
                                    showMessage("Password salah");
                                }
                            } else {
                                showMessage("Password salah");
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            dissmissPleasewaitDialog();
                            showMessage("Terjadi kesalahan, coba beberapa saat lagi");
                        }
                    });
                }
            }
        });
    }

    private void goToHome(Response body) {
        Anggota anggota = new Gson().fromJson(new Gson().toJson(body.getData()), Anggota.class);
        showMessage("Login berhasil, selamat datang, "+anggota.getNama());

        Preferences.setAnggota(getApplicationContext(), anggota);
        Preferences.setLoginFlag(getApplicationContext(), true);
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        finish();
    }

}
