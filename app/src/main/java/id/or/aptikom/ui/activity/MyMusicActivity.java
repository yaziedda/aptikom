package id.or.aptikom.ui.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;

public class MyMusicActivity extends AppCompatActivity {

    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.iv_play)
    ImageView ivPlay;
    MediaPlayer mpMars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_music);
        ButterKnife.bind(this);

        mpMars = MediaPlayer.create(this, R.raw.mars);

        ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mpMars.isPlaying()) {
                    mpMars.start();
                    ivPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.evp_action_pause));
                } else {
                    stopMars();
                }
            }
        });

    }

    private void stopMars() {
        if (mpMars.isPlaying()) {
            mpMars.stop();
            mpMars = MediaPlayer.create(MyMusicActivity.this, R.raw.mars);
            ivPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.evp_action_play));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMars();
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
