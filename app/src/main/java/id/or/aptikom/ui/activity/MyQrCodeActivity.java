package id.or.aptikom.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.image.ImageType;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.util.Preferences;

public class MyQrCodeActivity extends AppCompatActivity {

    @BindView(R.id.iv_qr)
    ImageView ivQr;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_valid)
    TextView tvValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_qr_code);
        ButterKnife.bind(this);

        setTitle("My Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Anggota anggota = Preferences.getAnggota(getApplicationContext());

        tvId.setText(anggota.getNoAnggota());
        tvName.setText(anggota.getNama());
        tvValid.setText("BERLAKU HINGGA : " +anggota.getMasaBerlaku());


        try {
            byte[] data = anggota.getNoAnggota().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
//            QRCode.from("asd").to(ImageType.PNG).bitmap()
            Bitmap myBitmap = QRCode.from(base64).withSize(400,400).to(ImageType.PNG).bitmap();
            ivQr.setImageBitmap(myBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
