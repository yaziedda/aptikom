package id.or.aptikom.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsNews;
import retrofit2.Call;
import retrofit2.Callback;

public class NewsDetailActivity extends AppCompatActivity {

    @BindView(R.id.iv_finish)
    ImageView ivFinish;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tv_title_2)
    TextView tvTitle2;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;
    @BindView(R.id.tv_content)
    TextView tvContent;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        id = getIntent().getIntExtra("id", 1);

        loadData();

        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        llLoading.setVisibility(View.VISIBLE);
        llReload.setVisibility(View.GONE);
        llContainer.setVisibility(View.GONE);
        mobileService.getMsNewsDetail(id).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                llLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        MsNews msNews = gson.fromJson(jsonObject.getAsJsonObject("data"), MsNews.class);
                        setDetail(msNews);
                        llContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    llReload.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                llLoading.setVisibility(View.GONE);
                llReload.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDetail(final MsNews model) {
        Glide.with(getApplicationContext())
                .load(model.getImage()).into(ivImage);

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ShowImageActivity.class);
                intent.putExtra("url", model.getImage());
                startActivity(intent);
            }
        });

        tvTitle.setText(model.getTitle());
        tvTitle2.setText(model.getTitle());
        tvDate.setText(model.getCreatedAt());
        Spanned spanned = Html.fromHtml(model.getContent());
        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        tvContent.setText(spanned);
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
