package id.or.aptikom.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;
import id.or.aptikom.adapter.ProductAdapter;
import id.or.aptikom.adapter.TrxProductAdapater;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.model.TrxProduct;
import id.or.aptikom.ui.activity.event.EventDetailActivity;
import id.or.aptikom.util.Preferences;
import retrofit2.Call;
import retrofit2.Callback;

public class PaymentHistoryActivity extends AppCompatActivity {

    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;
    @BindView(R.id.recycle_view)
    RecyclerView recycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_history);
        ButterKnife.bind(this);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        loadData();
        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        llLoading.setVisibility(View.VISIBLE);
        llReload.setVisibility(View.GONE);
        llContainer.setVisibility(View.GONE);
        mobileService.myPayment(Preferences.getAnggota(getApplicationContext()).getId()).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                llLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<TrxProduct> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<TrxProduct>>() {
                        }.getType());
                        setTOAdapter(listBody);
                        llContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    llReload.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                llLoading.setVisibility(View.GONE);
                llReload.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setTOAdapter(List<TrxProduct> listBody) {
        TrxProductAdapater adapter = new TrxProductAdapater(listBody, new TrxProductAdapater.OnItemClickListener() {
            @Override
            public void onItemClick(TrxProduct model) {
                Intent intent = new Intent(getApplicationContext(), ProductPaymentActivity.class);
                intent.putExtra("id", model.getId());
                startActivity(intent);
            }
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}