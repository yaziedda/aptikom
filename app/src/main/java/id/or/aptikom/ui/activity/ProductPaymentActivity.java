package id.or.aptikom.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.soundcloud.android.crop.Crop;

import net.glxn.qrgen.android.QRCode;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsConfig;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.model.TrxProduct;
import id.or.aptikom.ui.activity.event.EventDetailActivity;
import id.or.aptikom.util.BaseActivity;
import id.or.aptikom.util.IDRUtils;
import id.or.aptikom.util.PartUtils;
import id.or.aptikom.util.Static;
import id.zelory.compressor.Compressor;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class ProductPaymentActivity extends BaseActivity {

    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;
    int id;
    @BindView(R.id.tv_title_pay)
    TextView tvTitlePay;
    @BindView(R.id.ll_detail)
    LinearLayout llDetail;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_ticket_id)
    TextView tvTicketId;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_bank_account)
    TextView tvBankAccount;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_bank_an)
    TextView tvBankAn;
    @BindView(R.id.iv_upload)
    ImageView ivUpload;
    @BindView(R.id.bt_upload)
    Button btUpload;
    @BindView(R.id.bt_status)
    Button btStatus;
    Uri inputUri, outputUri;
    TrxProduct trxProductGlobal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_payment);
        ButterKnife.bind(this);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        id = getIntent().getIntExtra("id", 1);

        loadData();

        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        llLoading.setVisibility(View.VISIBLE);
        llReload.setVisibility(View.GONE);
        llContainer.setVisibility(View.GONE);
        mobileService.payProductDetail(id).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                llLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        final TrxProduct trxProduct = gson.fromJson(jsonObject.getAsJsonObject("data").get("trx_product"), TrxProduct.class);
                        trxProductGlobal = trxProduct;
                        final MsProduct msProduct = gson.fromJson(jsonObject.getAsJsonObject("data").get("product"), MsProduct.class);
                        MsConfig msConfig = gson.fromJson(jsonObject.getAsJsonObject("data").get("config"), MsConfig.class);

                        setDetail(trxProduct, msProduct, msConfig);

                        llContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    llReload.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                llLoading.setVisibility(View.GONE);
                llReload.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDetail(final TrxProduct trxProduct, final MsProduct msProduct, MsConfig msConfig) {
        tvTitlePay.setText(msProduct.getTitle());
        llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EventDetailActivity.class);
                intent.putExtra("id", msProduct.getId());
                intent.putExtra("from_payment", true);
                startActivity(intent);
            }
        });

        try {
            byte[] data = trxProduct.getTicketId().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Bitmap myBitmap = QRCode.from(base64).withSize(512,512).bitmap();
            ivQrCode.setImageBitmap(myBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvTicketId.setText(trxProduct.getTicketId());

        tvAmount.setText(IDRUtils.toRupiah(trxProduct.getAmount()));

        tvBankAccount.setText(msConfig.getBankNoRek());
        tvBankName.setText(msConfig.getBankName());
        tvBankAn.setText(msConfig.getBankAn());

        if(trxProduct.getProofOfPayment() == null || trxProduct.getProofOfPayment().isEmpty()){
            ivUpload.setVisibility(View.GONE);
            btUpload.setVisibility(View.VISIBLE);

            btUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPick();
                }
            });
        }else{
            ivUpload.setVisibility(View.VISIBLE);
            btUpload.setVisibility(View.GONE);
            final String url = Static.BASE_URL_IMAGE+trxProduct.getProofOfPayment();
            Glide.with(getApplicationContext())
                    .load(url).into(ivUpload);

            ivUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ShowImageActivity.class);
                    intent.putExtra("url", url);
                    startActivity(intent);
                }
            });
        }

        btStatus.setText(trxProduct.getStatusName());
        btStatus.setBackgroundColor(Color.parseColor("#"+trxProduct.getStatusColor()));
    }

    private void doCompres() {
        try {
            File file = new Compressor(ProductPaymentActivity.this)
                    .setMaxWidth(1280)
                    .setMaxHeight(720)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(new File(outputUri.getPath()));
            doUpload(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP) {

            doCompres();
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(ProductPaymentActivity.this);
                    if (photoFile != null) photoFile.delete();
                    Toast.makeText(getApplicationContext(), "Upload canceled", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.d("error", e.getMessage());
                Toast.makeText(getApplicationContext(), "Failed to upload cause something wrong, please try again", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onImagePicked(final File imageFile, EasyImage.ImageSource source, int type) {
                try {
                    inputUri = Uri.fromFile(imageFile);
                    outputUri = Uri.fromFile(new File(getCacheDir(), String.valueOf(System.currentTimeMillis() % 1000)));
                    Crop.of(inputUri, outputUri).start(ProductPaymentActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void doUpload(File file) {
        showPleasewaitDialog();
        MultipartBody.Part body = PartUtils.prepareFilePart("gambar", file);
        RequestBody trId = PartUtils.createPartFromString(String.valueOf(trxProductGlobal.getId()));

        final Map<String, RequestBody> map = new HashMap<>();
        map.put("id_trx", trId);

        mobileService.uploadBuktiTF(body, map)
                .enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        if (response.isSuccessful()) {
                            Response body = response.body();
                            if (body.getData() != null) {
                                Gson gson = new Gson();
                                JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                                final TrxProduct trxProduct = gson.fromJson(jsonObject.getAsJsonObject("data").get("trx_product"), TrxProduct.class);
                                final MsProduct msProduct = gson.fromJson(jsonObject.getAsJsonObject("data").get("product"), MsProduct.class);
                                MsConfig msConfig = gson.fromJson(jsonObject.getAsJsonObject("data").get("config"), MsConfig.class);

                                setDetail(trxProduct, msProduct, msConfig);
                            } else {
                                Toast.makeText(getApplicationContext(), Static.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), Static.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        Toast.makeText(getApplicationContext(), Static.SOMETHING_WRONG, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void showPick() {
        new MaterialDialog.Builder(ProductPaymentActivity.this)
                .items(new String[]{"CAMERA", "GALERY"})
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        if (position == 0) {
                            EasyImage.openCamera(ProductPaymentActivity.this, 1231);
                        } else if (position == 1) {
                            EasyImage.openGallery(ProductPaymentActivity.this, 1232);
                        }
                    }
                }).show();
    }

    public void toPick() {
        TedPermission.with(ProductPaymentActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showPick();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        finish();
                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA)
                .check();
    }
}
