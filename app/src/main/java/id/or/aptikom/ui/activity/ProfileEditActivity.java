package id.or.aptikom.ui.activity;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.util.BaseActivity;
import id.or.aptikom.util.Preferences;
import id.or.aptikom.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileEditActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.et_no_anggota)
    TextInputEditText etNoAnggota;
    @BindView(R.id.et_persone)
    TextInputEditText etPersone;
    @BindView(R.id.et_nama)
    TextInputEditText etNama;
    @BindView(R.id.et_univ)
    TextInputEditText etUniv;
    @BindView(R.id.et_jabatan)
    TextInputEditText etJabatan;
    @BindView(R.id.et_program)
    TextInputEditText etProgram;
    @BindView(R.id.et_alamat)
    TextInputEditText etAlamat;
    Anggota anggota;

    MobileService mobileService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        ButterKnife.bind(this);

        setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        anggota = Preferences.getAnggota(getApplicationContext());


        etNoAnggota.setText(anggota.getNoAnggota());
        etNoAnggota.setEnabled(false);
        etPersone.setText(anggota.getPersone());
        etNama.setText(anggota.getNama());
        etUniv.setText(anggota.getPerguruanTinggi());
        etJabatan.setText(anggota.getJabatan());
        etProgram.setText(anggota.getProgramStudi());
        etAlamat.setText(anggota.getAlamatPerguruanTinggi());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.iv_finish, R.id.bt_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_finish:
                finish();
                break;
            case R.id.bt_save:
                showPleasewaitDialog();

                String persone = etPersone.getText().toString();
                String nama = etNama.getText().toString();
                String univ = etUniv.getText().toString();
                String jabatan = etJabatan.getText().toString();
                String program = etProgram.getText().toString();
                String alamat = etAlamat.getText().toString();

                Map<String, String> map = new HashMap<>();
                map.put("persone", persone);
                map.put("nama", nama);
                map.put("univ", univ);
                map.put("jabatan", jabatan);
                map.put("program", program);
                map.put("alamat", alamat);

                mobileService.profileEdit(Preferences.getAnggota(getApplicationContext()).getId(), map)
                        .enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                dissmissPleasewaitDialog();
                                if(response.isSuccessful()){
                                    Response body = response.body();
                                    if (body.getData() != null) {
                                        Gson gson = new Gson();
                                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                                        Anggota model = gson.fromJson(jsonObject.getAsJsonObject("data"), Anggota.class);
                                        Preferences.setAnggota(getApplicationContext(), model);
                                        showMessage("Success to update profile");
                                    }else{
                                        showMessage(Static.SOMETHING_WRONG);
                                    }
                                }else{
                                    showMessage(Static.SOMETHING_WRONG);
                                }
                            }

                            @Override
                            public void onFailure(Call<Response> call, Throwable t) {
                                dissmissPleasewaitDialog();
                                showMessage(Static.SOMETHING_WRONG);
                            }
                        });
                break;
        }
    }
}
