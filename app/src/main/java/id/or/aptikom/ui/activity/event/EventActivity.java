package id.or.aptikom.ui.activity.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;
import id.or.aptikom.adapter.NewsAdapter;
import id.or.aptikom.adapter.ProductAdapter;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsNews;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.ui.activity.NewsDetailActivity;
import retrofit2.Call;
import retrofit2.Callback;

public class EventActivity extends AppCompatActivity {

    @BindView(R.id.iv_finish)
    ImageView ivFinish;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;
    @BindView(R.id.recycle_view)
    RecyclerView recycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        ButterKnife.bind(this);

        mobileService = ApiUtils.MobileService(getApplicationContext());

        loadData();
        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        llLoading.setVisibility(View.VISIBLE);
        llReload.setVisibility(View.GONE);
        llContainer.setVisibility(View.GONE);
        mobileService.getMsProduct().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                llLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<MsProduct> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MsProduct>>() {
                        }.getType());
                        setTOAdapter(listBody);
                        llContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    llReload.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                llLoading.setVisibility(View.GONE);
                llReload.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setTOAdapter(List<MsProduct> listBody) {
        ProductAdapter adapter = new ProductAdapter(listBody, new ProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MsProduct model) {
                Intent intent = new Intent(getApplicationContext(), EventDetailActivity.class);
                intent.putExtra("id", model.getId());
                startActivity(intent);
            }
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
