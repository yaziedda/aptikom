package id.or.aptikom.ui.activity.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.thefinestartist.Base;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsProduct;
import id.or.aptikom.model.TrxProduct;
import id.or.aptikom.ui.activity.LoginActivity;
import id.or.aptikom.ui.activity.ProductPaymentActivity;
import id.or.aptikom.ui.activity.ShowImageActivity;
import id.or.aptikom.util.BaseActivity;
import id.or.aptikom.util.IDRUtils;
import id.or.aptikom.util.Preferences;
import id.or.aptikom.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class EventDetailActivity extends BaseActivity {

    @BindView(R.id.iv_finish)
    ImageView ivFinish;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.tv_title_2)
    TextView tvTitle2;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_price)
    TextView tvPrice;
    @BindView(R.id.bt_bayar)
    Button btBayar;
    private int id;
    boolean fromPayment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);
        mobileService = ApiUtils.MobileService(getApplicationContext());

        id = getIntent().getIntExtra("id", 1);
        fromPayment = getIntent().getBooleanExtra("from_payment", false);

        loadData();

        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        if(fromPayment){
            btBayar.setVisibility(View.GONE);
        }else{
            btBayar.setVisibility(View.VISIBLE);
        }

        btBayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(EventDetailActivity.this)
                        .title("Konfirmasi")
                        .content("Apakah anda yakin akan melakukan pembayaran?")
                        .positiveText("Ya")
                        .negativeText("Tidak")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                showPleasewaitDialog();
                                Map<String, String> map = new HashMap<>();
                                map.put("id_user", String.valueOf(Preferences.getAnggota(getApplicationContext()).getId()));
                                map.put("id_product", String.valueOf(id));

                                mobileService.payProduct(map).enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                        dissmissPleasewaitDialog();
                                        if (response.isSuccessful()) {
                                            Response body = response.body();
                                            if (body.getData() != null) {
                                                Gson gson = new Gson();
                                                JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                                                TrxProduct model = gson.fromJson(jsonObject.getAsJsonObject("data").get("trx_product"), TrxProduct.class);

                                                Intent intent = new Intent(getApplicationContext(), ProductPaymentActivity.class);
                                                intent.putExtra("id", model.getId());
                                                startActivity(intent);
                                                finish();
                                            }else{
                                                showMessage(Static.SOMETHING_WRONG);
                                            }
                                        } else {
                                            showMessage(Static.SOMETHING_WRONG);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        dissmissPleasewaitDialog();
                                        showMessage(Static.SOMETHING_WRONG);
                                    }
                                });
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    private void loadData() {
        llLoading.setVisibility(View.VISIBLE);
        llReload.setVisibility(View.GONE);
        llContainer.setVisibility(View.GONE);
        mobileService.getMsProductDetail(id).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                llLoading.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        MsProduct model = gson.fromJson(jsonObject.getAsJsonObject("data"), MsProduct.class);
                        setDetail(model);
                        llContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    llReload.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                llLoading.setVisibility(View.GONE);
                llReload.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDetail(final MsProduct model) {
        if (model.getImage() != null && !model.getImage().isEmpty()) {
            Glide.with(getApplicationContext())
                    .load(model.getImage()).into(ivImage);
        } else {
            Glide.with(getApplicationContext())
                    .load(ContextCompat.getDrawable(getApplicationContext(), R.mipmap.ic_launcher)).into(ivImage);
        }

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ShowImageActivity.class);
                intent.putExtra("url", model.getImage());
                startActivity(intent);
            }
        });
        tvTitle.setText(model.getTitle());
        tvTitle2.setText(model.getTitle());
        tvDate.setText(model.getCreatedAt());
        tvPrice.setText(IDRUtils.toRupiah(model.getPrice()));
        Spanned spanned = Html.fromHtml(model.getDesc());
        tvContent.setMovementMethod(LinkMovementMethod.getInstance());
        tvContent.setText(spanned);
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}