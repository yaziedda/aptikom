package id.or.aptikom.ui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.glxn.qrgen.android.QRCode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.or.aptikom.R;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.util.Preferences;

public class AccountFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.tv_card_name)
    TextView tvCardName;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_valid)
    TextView tvValid;
    @BindView(R.id.tv_no_anggota)
    TextView tvNoAnggota;
    @BindView(R.id.tv_persone)
    TextView tvPersone;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_university)
    TextView tvUniversity;
    @BindView(R.id.tv_jabatan)
    TextView tvJabatan;
    @BindView(R.id.tv_prodi)
    TextView tvProdi;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Anggota anggota = Preferences.getAnggota(getContext());

        tvCardName.setText(anggota.getNama());
        tvValid.setText("BERLAKU HINGGA : " + anggota.getMasaBerlaku());

        tvNoAnggota.setText(anggota.getNoAnggota());
        tvPersone.setText(anggota.getPersone());
        tvName.setText(anggota.getNama());
        tvUniversity.setText(anggota.getPerguruanTinggi());
        tvJabatan.setText(anggota.getJabatan());
        tvProdi.setText(anggota.getProgramStudi());
        tvAddress.setText(anggota.getAlamatPerguruanTinggi());

        try {
            byte[] data = anggota.getNoAnggota().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Bitmap myBitmap = QRCode.from(base64).bitmap();
            ivQrCode.setImageBitmap(myBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // TODO: Rename method, update argument and hook method into UI event

}
