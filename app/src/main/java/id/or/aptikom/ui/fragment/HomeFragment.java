package id.or.aptikom.ui.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.glxn.qrgen.android.QRCode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.or.aptikom.R;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.model.MsHeadline;
import id.or.aptikom.ui.activity.MyQrCodeActivity;
import id.or.aptikom.util.Preferences;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    @BindView(R.id.slider)
    SliderLayout sliderLayout;
    Unbinder unbinder;
    @BindView(R.id.tv_card_name)
    TextView tvCardName;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_card_valid)
    TextView tvCardValid;
    @BindView(R.id.tv_no_anggota)
    TextView tvNoAnggota;
    @BindView(R.id.tv_persone)
    TextView tvPersone;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_university)
    TextView tvUniversity;
    @BindView(R.id.tv_jabatan)
    TextView tvJabatan;
    @BindView(R.id.tv_prodi)
    TextView tvProdi;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_welcome_name)
    TextView tvWelcomeName;
    @BindView(R.id.cv_card)
    CardView cvCard;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    MobileService mobileService;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mobileService = ApiUtils.MobileService(getContext());

        loadData();

        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        try{
            llLoading.setVisibility(View.VISIBLE);
            llReload.setVisibility(View.GONE);
            llContainer.setVisibility(View.GONE);
            mobileService.getMsHeadline().enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    llLoading.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        Response body = response.body();
                        if (body.getData() != null) {
                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                            List<MsHeadline> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MsHeadline>>() {
                            }.getType());

                            HashMap<String, String> url_maps = new HashMap<>();

                            for (MsHeadline msHeadline :
                                    listBody) {
                                url_maps.put(msHeadline.getTitle(), msHeadline.getImage());
                            }

                            setSlider(url_maps);
                            llContainer.setVisibility(View.VISIBLE);
                        }
                    } else {
                        llReload.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    llLoading.setVisibility(View.GONE);
                    llReload.setVisibility(View.VISIBLE);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setSlider(HashMap<String, String> url_maps) {
        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop);
//                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);

        Anggota anggota = Preferences.getAnggota(getContext());

        tvWelcomeName.setText(anggota.getNama());
        tvCardName.setText(anggota.getNama());
        tvCardValid.setText("BERLAKU HINGGA : " + anggota.getMasaBerlaku());

        tvNoAnggota.setText(anggota.getNoAnggota());
        tvPersone.setText(anggota.getPersone());
        tvName.setText(anggota.getNama());
        tvUniversity.setText(anggota.getPerguruanTinggi());
        tvJabatan.setText(anggota.getJabatan());
        tvProdi.setText(anggota.getProgramStudi());
        tvAddress.setText(anggota.getAlamatPerguruanTinggi());


        try {
            byte[] data = anggota.getNoAnggota().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Bitmap myBitmap = QRCode.from(base64).bitmap();
            ivQrCode.setImageBitmap(myBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        cvCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), MyQrCodeActivity.class));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
