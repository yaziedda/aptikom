package id.or.aptikom.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thefinestartist.finestwebview.FinestWebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.or.aptikom.R;
import id.or.aptikom.ui.activity.MyMusicActivity;
import id.or.aptikom.ui.activity.PaymentHistoryActivity;
import id.or.aptikom.ui.activity.event.EventActivity;
import id.or.aptikom.util.Static;

public class MyAptikomFragment extends Fragment {

    @BindView(R.id.cv_sejarah)
    CardView cvSejarah;
    @BindView(R.id.cv_org)
    CardView cvOrg;
    @BindView(R.id.cv_rules)
    CardView cvRules;
    @BindView(R.id.cv_music)
    CardView cvMusic;
    @BindView(R.id.cv_history)
    CardView cvHistory;
    @BindView(R.id.cv_payment)
    CardView cvPayment;
    Unbinder unbinder;

    public MyAptikomFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cv_sejarah, R.id.cv_org, R.id.cv_rules, R.id.cv_music, R.id.cv_history, R.id.cv_payment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_sejarah:
                new FinestWebView.Builder(getActivity()).show(Static.BASE_URL+"sejarah");
                break;
            case R.id.cv_org:
                new FinestWebView.Builder(getActivity()).show(Static.BASE_URL+"org");
                break;
            case R.id.cv_rules:
                new FinestWebView.Builder(getActivity()).show(Static.BASE_URL+"rules");
                break;
            case R.id.cv_music:
                startActivity(new Intent(getContext(), MyMusicActivity.class));
                break;
            case R.id.cv_history:
                startActivity(new Intent(getContext(), EventActivity.class));
                break;
            case R.id.cv_payment:
                startActivity(new Intent(getContext(), PaymentHistoryActivity.class));
                break;
        }
    }
}
