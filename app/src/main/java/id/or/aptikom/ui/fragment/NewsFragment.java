package id.or.aptikom.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.or.aptikom.R;
import id.or.aptikom.adapter.NewsAdapter;
import id.or.aptikom.client.ApiUtils;
import id.or.aptikom.client.mobile.MobileService;
import id.or.aptikom.client.model.Response;
import id.or.aptikom.model.MsNews;
import id.or.aptikom.ui.activity.NewsDetailActivity;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {


    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_reload)
    LinearLayout llReload;
    @BindView(R.id.ll_container)
    NestedScrollView llContainer;
    MobileService mobileService;

    Unbinder unbinder;
    @BindView(R.id.recycle_view)
    RecyclerView recycleView;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        startActivity(new Intent(getContext(), NewsDetailActivity.class));
        mobileService = ApiUtils.MobileService(getContext());

        loadData();

        llReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });
    }

    private void loadData() {
        try{
            llLoading.setVisibility(View.VISIBLE);
            llReload.setVisibility(View.GONE);
            llContainer.setVisibility(View.GONE);
            mobileService.getMsNews().enqueue(new Callback<Response>() {
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    llLoading.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        Response body = response.body();
                        if (body.getData() != null) {
                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                            List<MsNews> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MsNews>>() {
                            }.getType());
                            setTOAdapter(listBody);
                            llContainer.setVisibility(View.VISIBLE);
                        }
                    } else {
                        llReload.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    llLoading.setVisibility(View.GONE);
                    llReload.setVisibility(View.VISIBLE);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setTOAdapter(List<MsNews> listBody) {
        NewsAdapter adapter = new NewsAdapter(listBody, new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MsNews model) {
                Intent intent = new Intent(getContext(), NewsDetailActivity.class);
                intent.putExtra("id", model.getId());
                startActivity(intent);
            }
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
