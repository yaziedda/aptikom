package id.or.aptikom.ui.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.glxn.qrgen.android.QRCode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.or.aptikom.R;
import id.or.aptikom.model.Anggota;
import id.or.aptikom.ui.activity.LoginActivity;
import id.or.aptikom.ui.activity.ProfileEditActivity;
import id.or.aptikom.util.Preferences;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    @BindView(R.id.ll_Logout)
    LinearLayout llLogout;
    Unbinder unbinder;
    @BindView(R.id.tv_card_name)
    TextView tvCardName;
    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_valid)
    TextView tvValid;
    @BindView(R.id.ll_edit_profile)
    LinearLayout llEditProfile;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Anggota anggota = Preferences.getAnggota(getContext());

        tvCardName.setText(anggota.getNama());
        tvValid.setText("BERLAKU HINGGA : " + anggota.getMasaBerlaku());

        try {
            byte[] data = anggota.getNoAnggota().getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Bitmap myBitmap = QRCode.from(base64).bitmap();
            ivQrCode.setImageBitmap(myBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ll_edit_profile, R.id.ll_Logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_edit_profile:
                startActivity(new Intent(getContext(), ProfileEditActivity.class));
                break;
            case R.id.ll_Logout:
                new MaterialDialog.Builder(getContext())
                        .title("Konfirmasi")
                        .content("Apakah anda yakin akan logout?")
                        .positiveText("Ya")
                        .negativeText("Tidak")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Preferences.setLoginFlag(getContext(), false);
                                startActivity(new Intent(getContext(), LoginActivity.class));
                                getActivity().finish();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }
    }
}
